#include <stdarg.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>

uintptr_t rust_add(uintptr_t left, uintptr_t right);

uintptr_t rust_mul(uintptr_t left, uintptr_t right);
