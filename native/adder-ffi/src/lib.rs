#[no_mangle]
pub extern "C" fn rust_add(left: usize, right: usize) -> usize {
    adder::add(left, right)
}

#[no_mangle]
pub extern "C" fn rust_mul(left: usize, right: usize) -> usize {
    adder::mul(left, right)
}

