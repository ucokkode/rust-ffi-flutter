pub fn add(left: usize, right: usize) -> usize {
    left.wrapping_add(right)
}

pub fn mul(left: usize, right: usize) -> usize {
    left.wrapping_mul(right)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn it_works() {
        let result = add(2, 2);
        assert_eq!(result, 4);
    }
    #[test]
    fn it_works_too() {
        let result = mul(2, 2);
        assert_eq!(result, 4);
    }
}
