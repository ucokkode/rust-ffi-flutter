//
//  Generated file. Do not edit.
//

// clang-format off

#include "generated_plugin_registrant.h"

#include <adder/adder_plugin.h>

void fl_register_plugins(FlPluginRegistry* registry) {
  g_autoptr(FlPluginRegistrar) adder_registrar =
      fl_plugin_registry_get_registrar_for_plugin(registry, "AdderPlugin");
  adder_plugin_register_with_registrar(adder_registrar);
}
