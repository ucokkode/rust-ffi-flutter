mod error;

use crate::error::*;
use which::which;
use xshell::{cmd, Shell};

fn main() -> anyhow::Result<()> {
    simple_logger::init().unwrap();

    let shell = Shell::new()?;
    let plugin_name = "adder";
    let brige_crate_name = "adder-ffi";

    {
        log::info!("running cargo build: ");
        cmd!(shell, "cargo build -p {brige_crate_name}").run()?;
    }
    {
        log::info!("running cargo-ndk: ");
        let _p = shell.push_dir(format!("native/{brige_crate_name}"));
        which("cargo-ndk").or(Err(BinaryNotFoundKind::CargoNdk))?;
        let target = ["armeabi-v7a", "arm64-v8a", "x86_64"];
        let targets_flag: Vec<&str> = target
            .into_iter()
            .map(|target| ["-t", target])
            .flatten()
            .collect();
        let output_path = "../../packages/adder/android/src/main/jniLibs/";

        cmd!(
            shell,
            "cargo ndk {targets_flag...} -o {output_path} build --release"
        )
        .run()?;
    }
    {
        log::info!("generate dart code from header");
        let _p = shell.push_dir(format!("packages/{plugin_name}"));
        cmd!(shell, "flutter pub run ffigen").run()?;
    }
    {
        log::info!("run flutter");
        cmd!(shell, "flutter run").run()?;
    }
    Ok(())
}
