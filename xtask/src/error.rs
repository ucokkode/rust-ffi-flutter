use thiserror::Error;



#[derive(Error, Debug)]
pub enum BinaryNotFoundKind {
    #[error("cargo ndk not found, install the binary with `cargo install cargo-ndk`")]
    CargoNdk
}
