import 'package:flutter_test/flutter_test.dart';
import 'package:adder/adder.dart';
import 'package:adder/adder_platform_interface.dart';
import 'package:adder/adder_method_channel.dart';
import 'package:plugin_platform_interface/plugin_platform_interface.dart';

class MockAdderPlatform
    with MockPlatformInterfaceMixin
    implements AdderPlatform {

  @override
  Future<String?> getPlatformVersion() => Future.value('42');
}

void main() {
  final AdderPlatform initialPlatform = AdderPlatform.instance;

  test('$MethodChannelAdder is the default instance', () {
    expect(initialPlatform, isInstanceOf<MethodChannelAdder>());
  });

  test('getPlatformVersion', () async {
    Adder adderPlugin = Adder();
    MockAdderPlatform fakePlatform = MockAdderPlatform();
    AdderPlatform.instance = fakePlatform;

    expect(await adderPlugin.getPlatformVersion(), '42');
  });
}
