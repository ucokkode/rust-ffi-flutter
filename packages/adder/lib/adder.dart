import 'dart:ffi';

import 'package:adder/adder_method_channel.dart';

import "bindings.dart";
import 'dart:io' show Platform;

DynamicLibrary load({String basePath = ''}) {
  if (Platform.isAndroid || Platform.isLinux) {
    return DynamicLibrary.open('${basePath}libadder_ffi.so');
  } else if (Platform.isIOS) {
    // iOS is statically linked, so it is the same as the current process
    return DynamicLibrary.process();
  } else if (Platform.isMacOS) {
    return DynamicLibrary.open('${basePath}libadder_ffi.dylib');
  } else if (Platform.isWindows) {
    return DynamicLibrary.open('${basePath}libadder_ffi.dll');
  } else {
    throw NotSupportedPlatform('${Platform.operatingSystem} is not supported!');
  }
}

class NotSupportedPlatform implements Exception {
  NotSupportedPlatform(String s);
}

class Adder {
  late DynamicLibrary _lib;
  late MethodChannelAdder chan = MethodChannelAdder();
  Adder() {
    if (Platform.isWindows || Platform.isLinux || Platform.isMacOS) {
      _lib = load(basePath: 'target/debug/');
    } else {
      _lib = load();
    }
  }
  Future<String?> getPlatformVersion() async {
    return chan.getPlatformVersion();
  }

  int add(int a, int b) {
    return NativeLibrary(_lib).rust_add(a, b);
  }

  int mul(int a, int b) {
    return NativeLibrary(_lib).rust_mul(a, b);
  }
}
