import 'package:plugin_platform_interface/plugin_platform_interface.dart';

import 'adder_method_channel.dart';

abstract class AdderPlatform extends PlatformInterface {
  /// Constructs a AdderPlatform.
  AdderPlatform() : super(token: _token);

  static final Object _token = Object();

  static AdderPlatform _instance = MethodChannelAdder();

  /// The default instance of [AdderPlatform] to use.
  ///
  /// Defaults to [MethodChannelAdder].
  static AdderPlatform get instance => _instance;

  /// Platform-specific implementations should set this with their own
  /// platform-specific class that extends [AdderPlatform] when
  /// they register themselves.
  static set instance(AdderPlatform instance) {
    PlatformInterface.verifyToken(instance, _token);
    _instance = instance;
  }

  Future<String?> getPlatformVersion() {
    throw UnimplementedError('platformVersion() has not been implemented.');
  }
}
