import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';

import 'adder_platform_interface.dart';

/// An implementation of [AdderPlatform] that uses method channels.
class MethodChannelAdder extends AdderPlatform {
  /// The method channel used to interact with the native platform.
  @visibleForTesting
  final methodChannel = const MethodChannel('adder');

  @override
  Future<String?> getPlatformVersion() async {
    final version = await methodChannel.invokeMethod<String>('getPlatformVersion');
    return version;
  }
}
